#
# Cookbook Name:: test-nginx
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
package 'nginx' do
  action :install
end

service 'nginx' do
  action [:enable, :start]
end

package 'nodejs' do
  action :install
  options '--enablerepo=epel'
end

package 'npm' do
  action :install
  options '--enablerepo=epel'
end

cookbook_file '/usr/share/nginx/html/index.html' do
  source 'index.html'
  mode '0644'
end
